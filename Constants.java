package com.vijay.util;

/**
 * 
 * Constants to be used across the application
 *
 */
public class Constants {

	public static final int BASE_PREMIUM = 5000;
	public static final String GENDER = "male";
	public static final String YES = "yes";
	public static final String NO = "no";

}
