package com.vijay.application;

import java.util.Scanner;

import com.vijay.model.ApplicantInfo;
import com.vijay.util.Constants;

/**
 * 
 * Main class for the QuoteGeneration Application
 *
 */

public class QuoteGenerator {

	public static void main(String[] args) {

		ApplicantInfo applicantData = new ApplicantInfo();
		Scanner dataReader = new Scanner(System.in);

		System.out.println("Please enter Applicant name");
		String name = dataReader.next();
		applicantData.setApplicantName(name);

		System.out.println("Please enter Applicant Age");
		int age = dataReader.nextInt();
		applicantData.setApplicantAge(age);

		System.out.println("Please enter Applicant Gender");
		String gender = dataReader.next();
		applicantData.setApplicantGender(gender);

		System.out.println("Does Applicant has Hypertension: (True/False)");
		String hypertension = dataReader.next();
		applicantData.setHyperTension(hypertension);

		System.out.println("Does Applicant has BloodPressure: (True/False)");
		String bloodPressure = dataReader.next();
		applicantData.setBloodPressure(bloodPressure);

		System.out.println("Does Applicant has BloodSugar: (Enter Yes or No)");
		String bloodSugar = dataReader.next();
		applicantData.setBloodSugar(bloodSugar);

		System.out.println("Does Applicant is  Overweight: (Enter Yes or No)");
		String overWeight = dataReader.next();
		applicantData.setOverWeight(overWeight);

		System.out.println("Does Applicant does smoking: (Enter Yes or No)");
		String smoking = dataReader.next();
		applicantData.setSmoking(smoking);

		System.out.println("Does Applicant use Alchocol: (Enter Yes or No)");
		String alchol = dataReader.next();
		applicantData.setAlcohol(alchol);

		System.out.println("Does Applicant does  DailyExercise: (Enter Yes or No)");
		String dailyExercise = dataReader.next();
		applicantData.setDailyExercise(dailyExercise);

		System.out.println("Does Applicant use drugs: (Enter Yes or No)");
		String drugs = dataReader.next();
		applicantData.setDrugs(drugs);

		int finalPremium = calculatePremium(applicantData);
		System.out.println("QuoteGenerator.main()" + finalPremium);
		dataReader.close();
	}

	/**
	 *This Method calculates the over all premium of the applicant 
	 * @param applicantInfo
	 */

	public static int calculatePremium(ApplicantInfo applicantInfo) {
		int totalPremium = 0;
		totalPremium = premiumCalculationforAge(applicantInfo) + premiumCalculationforGender(applicantInfo)
				+ premiumCalculationforPreExistingConditions(applicantInfo)
				+ premiumCalculationforHabits(applicantInfo);

		System.out
				.println("Health Insurance Premium For Mr." + applicantInfo.getApplicantName() + "is :" + totalPremium);

		return totalPremium;
	}

	/**
	 * This Method calculates the premium based on the age 
	 * 
	 * @param applicantInfo
	 * @return premiumBasedOnAge
	 */
	public static int premiumCalculationforAge(ApplicantInfo applicantInfo) {

		int premiumBasedOnAge = 0;

		if (applicantInfo.getApplicantAge() < 18) {
			premiumBasedOnAge = Constants.BASE_PREMIUM;
		} else if (applicantInfo.getApplicantAge() > 18 && applicantInfo.getApplicantAge() <= 40) {
			premiumBasedOnAge = Constants.BASE_PREMIUM + (Constants.BASE_PREMIUM * 10 / 100);
		} else if (applicantInfo.getApplicantAge() > 40) {
			premiumBasedOnAge = Constants.BASE_PREMIUM + (Constants.BASE_PREMIUM * 20 / 100);
		}
		System.out.println("QuoteGenerator.premiumCalculationforAge()" + premiumBasedOnAge);
		return premiumBasedOnAge;

	}

	/**
	 * This method checks if the applicant gender is male and adds 2% of the base
	 * premium only if the applicant is male
	 * 
	 * @param applicantInfo
	 * @return premiumBasedOnGender
	 */
	public static int premiumCalculationforGender(ApplicantInfo applicantInfo) {
		int premiumBasedOnGender = 0;
		if (Constants.GENDER.equalsIgnoreCase(applicantInfo.getApplicantGender())) {
			premiumBasedOnGender = (Constants.BASE_PREMIUM * 2 / 100);
		}
		System.out.println("QuoteGenerator.premiumCalculationforGender()" + premiumBasedOnGender);
		return premiumBasedOnGender;
	}

	/**
	 * This method calcuates the premium based on pre existign conditions of the applicant
	 * @param applicantInfo
	 * @return premiumBasedOnPreExistingConditions
	 */
	public static int premiumCalculationforPreExistingConditions(ApplicantInfo applicantInfo) {
		int premiumBasedOnPreExistingConditions = 0;
		int hyperTensionPremium = 0;
		int bloodPressurePremium = 0;
		int bloodSugarPremium = 0;
		int overWeightPremium = 0;

		if (Constants.YES.equalsIgnoreCase(applicantInfo.getHyperTension())) {
			hyperTensionPremium = (Constants.BASE_PREMIUM * 1 / 100);
		} else if (Constants.YES.equalsIgnoreCase(applicantInfo.getBloodPressure())) {
			bloodPressurePremium = (Constants.BASE_PREMIUM * 1 / 100);
		} else if (Constants.YES.equalsIgnoreCase(applicantInfo.getBloodSugar())) {
			bloodSugarPremium = (Constants.BASE_PREMIUM * 1 / 100);
		} else if (Constants.YES.equalsIgnoreCase(applicantInfo.getOverWeight())) {
			overWeightPremium = (Constants.BASE_PREMIUM * 1 / 100);
		}
		premiumBasedOnPreExistingConditions = hyperTensionPremium + bloodPressurePremium + bloodSugarPremium
				+ overWeightPremium;
		System.out.println(
				"QuoteGenerator.premiumCalculationforPreExistingConditions()" + premiumBasedOnPreExistingConditions);
		return premiumBasedOnPreExistingConditions;
	}
	
	/**
	 * This method calcuates the premium based on Habits of the applicant
	 * @param applicantInfo
	 * @return premiumBasedOnHabits
	 */

	public static int premiumCalculationforHabits(ApplicantInfo applicantInfo) {
		int premiumBasedOnHabits = 0;
		int smokingPremium = 0;
		int alchocolPremium = 0;
		int dailyExercisePremium = 0;
		int drugsPremium = 0;

		smokingPremium = Constants.YES.equalsIgnoreCase(applicantInfo.getSmoking()) ? (Constants.BASE_PREMIUM * 3 / 100)
				: (Constants.BASE_PREMIUM * -3 / 100);
		alchocolPremium = Constants.YES.equalsIgnoreCase(applicantInfo.getAlcohol())
				? (Constants.BASE_PREMIUM * 3 / 100)
				: (Constants.BASE_PREMIUM * -3 / 100);
		dailyExercisePremium = Constants.YES.equalsIgnoreCase(applicantInfo.getDailyExercise())
				? (Constants.BASE_PREMIUM * -3 / 100)
				: (Constants.BASE_PREMIUM * 3 / 100);
		drugsPremium = Constants.YES.equalsIgnoreCase(applicantInfo.getDrugs()) ? (Constants.BASE_PREMIUM * 3 / 100)
				: (Constants.BASE_PREMIUM * -3 / 100);

		premiumBasedOnHabits = smokingPremium + alchocolPremium + dailyExercisePremium + drugsPremium;
		System.out.println("QuoteGenerator.premiumCalculationforHabits()" + premiumBasedOnHabits);
		return premiumBasedOnHabits;
	}

}