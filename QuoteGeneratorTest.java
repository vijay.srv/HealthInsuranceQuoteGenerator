package com.vijay.test;

import static org.junit.Assert.*;
import org.junit.Test;
import com.vijay.application.QuoteGenerator;
import com.vijay.model.ApplicantInfo;

/**
 * 
 * Junit tests for testing the HealthInsurance quote application methods
 *
 */

public class QuoteGeneratorTest {

	ApplicantInfo info = null;
	
	 /**
	  *  This Method is to test the whole program by passing the sample inputs given
	  */
	
	@Test
	public void testCalculatePremium() {
	
		info = new ApplicantInfo();
		
		info.setApplicantAge(34);
		info.setApplicantGender("male");
		info.setApplicantName("Norman Gomes");
		
		info.setHyperTension("no");
		info.setBloodPressure("no");
		info.setOverWeight("yes");
		info.setBloodSugar("no");
		
		info.setSmoking("no");
		info.setAlcohol("yes");
		info.setDailyExercise("yes");
		info.setDrugs("no");
		
		assertEquals(5350, QuoteGenerator.calculatePremium(info));
	}

	/**
	 * This test is to check the premium for the applicants based on age if(age <17)
	 * then Premium is 5000, if not the test will fail
	 */

	@Test
	public void testPremiumCalculationforAge() {
	    info = new ApplicantInfo();
		info.setApplicantAge(17);
		assertEquals(5000, QuoteGenerator.premiumCalculationforAge(info));
	}

	/**
	 * This test is to ensure that for the male applicant the premium is 2 % more
	 * from the base premium
	 */

	@Test
	public void testPremiumCalculationforGender() {
		info = new ApplicantInfo();
		info.setApplicantAge(17);
		info.setApplicantGender("male");
		assertEquals(100, QuoteGenerator.premiumCalculationforGender(info));
	}

	/**
	 * This test is to ensure person with no pre-existing conditions will not get
	 * extra premium
	 */

	@Test
	public void testPremiumCalculationforPreExistingConditions() {
		info = new ApplicantInfo();
		info.setHyperTension("no");
		info.setBloodPressure("no");
		info.setBloodSugar("no");
		info.setOverWeight("no");
		assertEquals(0, QuoteGenerator.premiumCalculationforPreExistingConditions(info));
	}

	/**
	 * This test ensures that people with 2 bad habits and 2 good habits, there will
	 *  change in premium
	 */

	@Test
	public void testPremiumCalculationforHabits() {
		info = new ApplicantInfo();
		info.setAlcohol("yes");
		info.setDailyExercise("yes");
		info.setDrugs("no");
		info.setSmoking("no");
		assertEquals(-300, QuoteGenerator.premiumCalculationforHabits(info));
	}
}
